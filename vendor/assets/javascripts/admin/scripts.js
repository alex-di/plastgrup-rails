function initializeJS() {
    console.log("Init js")

    //tool tips
    jQuery('.tooltips').tooltip();

    //popovers
    jQuery('.popovers').popover();

    //sidebar dropdown menu
    jQuery('#sidebar .sub-menu > a').click(function () {
        jQuery(this).next().slideToggle()
    });

    if(location.hash !== "")
        jQuery("[href='" + location.hash + "']").click()

    // sidebar menu toggle
    jQuery(function() {
        function responsiveView() {
            var wSize = jQuery(window).width();
            if (wSize <= 768) {
                jQuery('#container').addClass('sidebar-close');
                jQuery('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                jQuery('#container').removeClass('sidebar-close');
                jQuery('#sidebar > ul').show();
            }
        }
        jQuery(window).on('load', responsiveView);
        jQuery(window).on('resize', responsiveView);
    });

    jQuery('.toggle-nav').click(function () {
        if (jQuery('#sidebar > ul').is(":visible") === true) {
            jQuery('#main-content').css({
                'margin-left': '0px'
            });
            jQuery('#sidebar').css({
                'margin-left': '-180px'
            });
            jQuery('#sidebar > ul').hide();
            jQuery("#container").addClass("sidebar-closed");
        } else {
            jQuery('#main-content').css({
                'margin-left': '180px'
            });
            jQuery('#sidebar > ul').show();
            jQuery('#sidebar').css({
                'margin-left': '0'
            });
            jQuery("#container").removeClass("sidebar-closed");
        }
    });

    jQuery("[name^='product[factoids_attributes]'][name$='[type]']").on("change", function(e){
        var el = jQuery(e.currentTarget)
        ,   container = el.parents(".factoid-container")

        switch(el.val()){
            case "Клиент":
                container.find(".form-group.client").show()
                container.find(".form-group.title").hide()
                container.find(".form-group.file").hide()
                break;

            case "Пустой":
                container.find(".form-group.client").hide()
                container.find(".form-group.file").hide()
                container.find(".form-group.description").hide()
                break;

            case "ГОСТ":
                container.find(".form-group.description").hide()
                container.find(".form-group.file").show()
                break;

            default:
                container.find(".form-group.description").show()
                container.find(".form-group.client").hide()
                container.find(".form-group.title").show()
                container.find(".form-group.file").hide()

        }

        container.find("")
    })

}

jQuery(document).ready(function(){
    initializeJS();
});
