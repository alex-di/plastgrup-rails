class Attachment < ActiveRecord::Base
	belongs_to :product
	self.inheritance_column = nil


	mount_uploader :file, AttachmentUploader

	validates :name, :presence => true
	validates :file, :presence => true
	validates :type, :presence => true

	def types_list
		{ "Всплывающее окно"=> :popup ,  "Скачать" => :download , "В новом окне" => :blank }
	end

end
