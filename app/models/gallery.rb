class Gallery < ActiveRecord::Base
    belongs_to :imagable, polymorphic: true
    belongs_to :image
end
