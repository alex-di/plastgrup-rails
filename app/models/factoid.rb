class Factoid < ActiveRecord::Base
	belongs_to :product
	belongs_to :client
	self.inheritance_column = nil

    mount_uploader :file, ImageUploader

	def types_list
		["ГОСТ", "Обычный", "Параметры", "Клиент", "Пустой"]
	end
end
