class Complect < ActiveRecord::Base
	belongs_to :product
	self.inheritance_column = nil

	serialize :coords, JSON

	def types_list
		["Базовая", "Дополнительная"]
	end
end
