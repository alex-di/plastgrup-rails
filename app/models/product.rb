class Product < ActiveRecord::Base
	has_and_belongs_to_many :categories
	has_many :factoids
	has_many :complects
	has_many :attachments
	has_many :properties
	has_many :galleries, as: :imagable
	has_many :images, through: :galleries
	mount_uploader :complect_image, ImageUploader

	accepts_nested_attributes_for :factoids, :allow_destroy => true
	accepts_nested_attributes_for :complects, :allow_destroy => true
	accepts_nested_attributes_for :attachments, :allow_destroy => true
	accepts_nested_attributes_for :properties, :allow_destroy => true

	validates :name, :presence => true
	validates :slug, :presence => true

	serialize :properties_options, Array
	serialize :meta_keywords, Array
	serialize :search_tags, Array


	extend FriendlyId
	friendly_id :name, :use => [:slugged, :history, :finders]

	def normalize_friendly_id(text)
		text.to_slug.normalize(transliterations: :russian).to_s
	end


	def self.get_fields_avalible
		arr = self.select(:properties_options)
		.map(&:properties_options)
		.flatten.compact
		.map do |obj|
			obj["fields"]
		end
		.flatten.compact.uniq
	end

	def self.get_metakeywords
		arr = self.select(:meta_keywords)
		.map(&:meta_keywords)
		.flatten.compact.uniq
	end

	scope :without_cat, -> {joins(:categories).where(:categories => {:id => nil})}

	scope :used, -> {where(:using => true)}

end
