class Image < ActiveRecord::Base

    has_many :galleries, dependent: :destroy
    has_many :product, :through => :galleries, :source => :imagable, :source_type => "Product"
    has_many :categories, :through => :galleries, :source => :imagable, :source_type => "Category"

    mount_uploader :file, ImageUploader

    validates :file, :presence => true

	scope :shared, -> {where(:display => true)}

end
