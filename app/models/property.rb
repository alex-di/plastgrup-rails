class Property < ActiveRecord::Base
    belongs_to :product

    mount_uploader :image, ImageUploader
    mount_uploader :dwg, AttachmentUploader
    mount_uploader :pdf, AttachmentUploader
    serialize :values, Array
end
