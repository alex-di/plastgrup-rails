class Category < ActiveRecord::Base
	has_and_belongs_to_many :products
	has_many :attachments, through: :products
	has_many :galleries, as: :imagable
	has_many :images, through: :galleries
	has_many :products_images, as: :gallery, through: :products, source: :images

	validates :name, :presence => true
	validates :slug, :presence => true

	serialize :meta_keywords, Array
	serialize :search_tags, Array

	extend FriendlyId
	friendly_id :name, :use => [:slugged, :history, :finders]

	def normalize_friendly_id(text)
		text.to_slug.normalize(transliterations: :russian).to_s
	end

	def self.get_metakeywords
		arr = self.select(:meta_keywords)
		.map(&:meta_keywords)
		.flatten.compact.uniq
	end


end
