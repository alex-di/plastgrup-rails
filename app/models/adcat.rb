class Adcat < ActiveRecord::Base

	validates :name, :presence => true
	validates :html, :presence => true
end
