class Admin::ImagesController < ApplicationController

	before_action :require_user
	layout "admin"

    def index
        @images = Image.all

        respond_to do |format|
            format.html
            format.json {render json: @images}
        end
    end

    def create
        @image = Image.new image_params

        if @image.save
            render json: @image
        else
            render json: {:errors => @image.errors}
        end
    end

	def edit
		@image = Image.find(params[:id])
		respond_to do |format|
			format.html do ||
				if request.xhr?
					render :layout => nil
				end
			end
		end

	end

	def update

		@image = Image.find(params[:id])
		if @image.update image_params
			redirect_to admin_images_path
		else
			render "admin/images/edit"
		end

	end

    private
    def image_params
        params.require(:image).permit(:file, :text, :text_preview, :display)
    end
end
