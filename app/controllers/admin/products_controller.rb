class Admin::ProductsController < ApplicationController
	before_action :require_user

	layout "admin"

	def new
		@product = Product.new
		@image = Image.new
	end

	def create
		@product = Product.new product_params
		@product.images = Image.where(id: params[:product][:images])
		@product.factoids.new
		@product.properties_options = [{
			:basic => true,
			:name => "Основная",
			:fields => []
		}]
		if @product.save
			redirect_to edit_admin_product_path(@product)
		else
			flash[:errors] = @product.errors
			Rails.logger.debug(@product.errors.to_json)
			render "admin/products/new"
		end
	end

	def edit
		@product = Product.friendly.find(params[:id])
		@product.properties_options.push({
		 	"basic" => true,
		 	"name" => "Основная",
		 	"fields" => []
		 }) if @product.properties_options.empty?
		@image = Image.new
	end

	def update
		@product = Product.find(params[:id])
		@product.images = Image.where(id: params[:product][:images])
		if @product.update(product_params)
			redirect_to admin_products_path
		else
			flash[:errors] = @product.errors
			render "admin/products/edit"
		end
	end

	def index
		@products = Product.all
	end

	def destroy
		@product = Product.find(params[:id])
		@product.destroy
		redirect_to admin_products_path
	end

	private
	def product_params

	    params.require(:product).permit(:name, :slogan, :subtitle, :description, :images, :complect_image, :questionare_file, :properties_unit, :active, :visible, :using, :email,
			:meta_title, :meta_description, :listname,
			category_ids: [],
			meta_keywords: [],
			search_tags: [],
			factoids_attributes: [:title, :description, :type, :id, :client_id, :position],
			complects_attributes: [:title, :description, :type, :id, :feature, :coords],
			attachments_attributes: [:name, :file, :id, :group, :type],
			properties_attributes: [:title, :values, :id, :image, :menu_title]
	    	).tap do |whitelisted|
				whitelisted[:properties_options] = params[:product][:properties_options]

				unless whitelisted[:properties_options].nil?
					whitelisted[:properties_options].map do |opt|
						opt["fields"] = opt["fields"].split(",") if opt["fields"].is_a? String
					end
				end
			end

	end

end
