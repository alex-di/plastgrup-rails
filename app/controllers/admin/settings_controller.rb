class Admin::SettingsController < ApplicationController
	before_action :require_user

	require "settings"

	include Settings

	def get
		settings_get(params[:setting])
		redirect_to admin_root
	end

	def set
	end
end
