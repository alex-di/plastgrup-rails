class Admin::AdcatsController < ApplicationController


	before_action :require_user
	layout "admin"

	def index
		@categories = Adcat.order :name
	end 

	def new 
		@category = Adcat.new 
	end

	def create 
		@category = Adcat.new(adcat_params)
		if @category.save
			redirect_to edit_admin_adcat_path(@category)
		else
			redirect_to new_admin_adcat_path
		end
	end 

	def show
		@category = Adcat.find(params[:id])
	end

	def edit
		@category = Adcat.find(params[:id])
	end


	def update 
		@category = Adcat.find(params[:id])
		if @category.update(adcat_params)
			redirect_to admin_adcats_path(@category)
		else
			redirect_to edit_admin_adcat_path(@category)
		end
	end 

	def destroy
		@category = Adcat.find(params[:id])
		@category.destroy
		redirect_to admin_adcats_path
	end




	private 
	def adcat_params
	    params.require(:adcat).permit(:name, :html)
	end

end
