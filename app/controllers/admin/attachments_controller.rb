class Admin::AttachmentsController < ApplicationController

	before_action :require_user
	layout "admin"

	def new
		@attachment = Attachment.new
	end

	def create
		@attachment = Attachment.new(attachment_params)
		@product = Product.friendly.find(params[:product_id])
		@attachment.product = @product

		if @attachment.save
			redirect_to edit_admin_product_path(@attachment.product_id)
		else
			render :template => "admin/products/#{@product.id}/attachments/new"
		end
	end

	def destroy
	end

	private
	def attachment_params
		params.require(:attachment).permit(:name, :file, :type, :group, :product_id)
	end
end
