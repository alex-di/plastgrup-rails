class Admin::PropertiesController < ApplicationController
    before_action :require_user
    layout "admin"

    def new
        @property = Property.new
        @property.product = Product.friendly.find(params[:product_id])
    end

    def create

        @property = Property.new(property_params)
        @property.product = Product.friendly.find(params[:product_id])

        if @property.save
            redirect_to edit_admin_product_path(@property.product_id)
        else
            redirect_to new_admin_product_property_path(params[:product_id])
        end
    end

    def edit
        @property = Property.find(params[:id])
    end

    def update
        @property = Property.find(params[:id])

        if @property.update(property_params)
            redirect_to edit_admin_product_path(@property.product_id)
        else
            redirect_to new_admin_product_property_path(params[:product_id])
        end
    end

    def destroy
        @property = Property.find(params[:id])
        @property.destroy

        redirect_to edit_admin_product_path(params[:product_id])
    end

    private
    def property_params
        params.require(:property).permit(:title, :image, :pdf, :dwg, :subtitle, :option,
            values: []
        )
    end
end
