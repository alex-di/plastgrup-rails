class Admin::DealersController < ApplicationController

	before_action :require_user
	layout "admin"
    # GET /dealers
    # GET /dealers.json
    def index
      @dealers = Dealer.all
    end


    # GET /dealers/new
    def new
      @dealer = Dealer.new
    end

    # GET /dealers/1/edit
    def edit
		@dealer = Dealer.find(params[:id])
    end

    # POST /dealers
    # POST /dealers.json
    def create
      @dealer = Dealer.new(dealer_params)

      respond_to do |format|
        if @dealer.save
          format.html { redirect_to edit_admin_dealer_path @dealer, notice: 'Dealer was successfully created.' }
          format.json { render :show, status: :created, location: @dealer }
        else
          format.html { render :new }
          format.json { render json: @dealer.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /dealers/1
    # PATCH/PUT /dealers/1.json
    def update
		@dealer = Dealer.find(params[:id])
      respond_to do |format|
        if @dealer.update(dealer_params)
          format.html { redirect_to edit_admin_dealer_path @dealer, notice: 'Dealer was successfully updated.' }
          format.json { render :show, status: :ok, location: @dealer }
        else
          format.html { render :edit }
          format.json { render json: @dealer.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /dealers/1
    # DELETE /dealers/1.json
    def destroy
		@dealer = Dealer.find(params[:id])
      @dealer.destroy
      respond_to do |format|
        format.html { redirect_to admin_dealers_path, notice: 'Dealer was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

      # Never trust parameters from the scary internet, only allow the white list through.
      def dealer_params
        params.require(:dealer).permit(:name, :city, :email, :phone, :address, :active)
      end
end
