

class Admin::AdminController < ApplicationController
	before_action :require_user
	layout "admin"

	include Setng

	def index
		@site_name = Setng::get("site_name")
		@cat_order = Setng::get("cat_order")
		@search_tags = Setng::get("search_tags")
		@main_list_using = Setng::get("main_list_using")
		@main_list_order = Setng::get("main_list_order")
		@main_list_examples = Setng::get("main_list_examples")
		@main_product = Setng::get("main_product")
		@main_menu = Setng::get("main_menu")
		# console
	end

	def set
		Setng::set(params.require(:parameter), params[:value])
		redirect_to admin_root_path
	end
end
