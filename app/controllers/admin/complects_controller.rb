class Admin::ComplectsController < ApplicationController

	before_action :require_user
	layout "admin"

	def new
		@complect = Complect.new

	end

	def create

		@complect = Complect.new(complect_params)
		@complect.product = Product.friendly.find(params[:product_id])

		if @complect.save
			redirect_to edit_admin_product_path(@complect.product_id)
		else
			redirect_to new_admin_product_complect_path(params[:product_id])
		end
	end

	def destroy
		@complect = Complect.find(params[:id])
		@complect.destroy

		redirect_to edit_admin_product_path(params[:product_id])
	end

	private
	def complect_params
		params.require(:complect).permit(:title, :description, :type, :product_id)
	end
end
