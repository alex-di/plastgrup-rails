class Admin::CategoriesController < ApplicationController

	before_action :require_user
	layout "admin"

	def index
		@categories = Category.order :name
	end

	def new
		@category = Category.new
	end

	def create
		@category = Category.new(category_params)
		@category.images = Image.where(id: params[:category][:images])
		if @category.save
			redirect_to edit_admin_category_path(@category)
		else
			render :template => "admin/categories/new"
		end
	end

	def edit
		@category = Category.friendly.find(params[:id])
	end


	def update
		@category = Category.friendly.find(params[:id])
		@category.images = Image.where(id: params[:category][:images])
		if @category.update(category_params)
			redirect_to edit_admin_category_path(@category)
		else
			redirect_to edit_admin_category_path(@category)
		end
	end

	def destroy
		@category = Category.friendly.find(params[:id])

		if @category.products.any?
			flash[:notice] = "Невозможно удалить категорию, пока к ней привязаны продукты"
			redirect_to edit_admin_category_path @category.id
		else
			@category.destroy
			redirect_to admin_categories_path
		end
	end

	private
	def category_params
	    params.require(:category).permit(:name, :description, :meta_title, :meta_description, :slug, :active, :visible, :images, :email,
		meta_keywords: [],
		search_tags: [])
	end


end
