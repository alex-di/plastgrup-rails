class DealersController < ApplicationController
    
    # GET /dealers/1
    # GET /dealers/1.json
    def show
      @dealer = Dealer.find(params[:id])
    end

end
