class ProductsController < ApplicationController
	def index
	end

	def show

		@product = Product.friendly.find(params[:id])

		if !@product.active && !current_user
			render "errors/access"
		end

		if request.path != product_path(@product)
			return redirect_to @product, :status => :moved_permanently
		end

		prepare_meta_tags	title: @product.meta_title.presence || @product.name,
							description: @product.meta_description.presence || @product.description,
							keywords: @product.meta_keywords.reject {|k| k == ''} || []

	end

end
