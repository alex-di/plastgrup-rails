class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.


  I18n.locale = :ru
  protect_from_forgery with: :exception

	helper_method :current_user

	def current_user
		@current_user ||= User.find(session[:user_id]) if session[:user_id]
	end

	def require_user
		redirect_to '/admin/login?redirect=' + request.path unless current_user
	end

    before_action :prepare_meta_tags, if: "request.get?"

    def prepare_meta_tags(options={})
        site_name   = "Пластгрупп"
        title = nil
        description = nil
        image       = options[:image] || nil
        current_url = request.url

        # Let's prepare a nice set of defaults
        defaults = {
          site:        site_name,
          title:       title,
          image:       image,
          description: description,
          keywords:    %w[web software development mobile app],
          twitter: {
            site_name: site_name,
            site: '@thecookieshq',
            card: 'summary',
            description: description,
            image: image
          },
          og: {
            url: current_url,
            site_name: site_name,
            title: title,
            image: image,
            description: description,
            type: 'website'
          }
        }

        options.reverse_merge!(defaults)

        set_meta_tags options
    end


	require "settings"

end
