class AttachmentsController < ApplicationController
    def download
        @attachment = Attachment.find(params[:id])
        dsp = @attachment.type == "blank" ? "inline" : "attachment"
        send_file @attachment.file.path, {:disposition => dsp }
    end
end
