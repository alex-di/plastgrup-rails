class CategoriesController < ApplicationController

	include Setng

	caches_action :search, expires_in: 1.hour

	def search
		@cat_order = Setng::get("cat_order").split("|")
		@categories = []
		used_cat_ids = []
		used_adcat_ids = []
		used_product_ids = []

		@search_tags = Setng::get("search_tags")

		@cat_order.each do |item|
			tmp = item.split("/")
			type = tmp[0]
			id = tmp[1]

			if type == "adcat"
				@categories.push Adcat.find(id)
				used_adcat_ids.push(id)
			elsif type == "cat"
				category = Category.find(id)
				category.products = category.products.to_a.reject do |p|
					!p.visible
				end

				@categories.push category
				used_cat_ids.push(id)
			elsif type == "product"
				@categories.push Product.find(id)
				used_product_ids.push(id)
			end
		end

		Category.all.each do |cat|
			unless used_cat_ids.include? cat.id.to_s

				cat.products = cat.products.to_a.reject do |p|
					!p.visible
				end
				@categories.push cat
			end
		end

		Product.without_cat.each do |cat|
			@categories.push cat unless used_product_ids.include? cat.id.to_s
		end

		Adcat.all.each do |cat|
			@categories.push cat unless used_adcat_ids.include? cat.id.to_s
		end

	end

	def show

		@category = Category.friendly.find(params[:id])


		if !@category.active && !current_user
			render "errors/access"
		end

		if request.path != category_path(@category)
			return redirect_to @category, :status => :moved_permanently
		end

		@attachments = {}
		Rails.logger.debug(@cateogory)
		@category.attachments.each do |a|
			@attachments[a.group] ||= []
			@attachments[a.group].push a
		end


		prepare_meta_tags	title: @category.meta_title.presence || @category.name,
							description: @category.meta_description.presence || @category.description,
							keywords: @category.meta_keywords.reject {|k| k == ''} || []


	end
end
