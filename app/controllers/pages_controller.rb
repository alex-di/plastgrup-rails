class PagesController < ApplicationController

    def template1
    end

    def template2
    end

    def index
    end


	caches_action :main, expires_in: 1.hour

    def main

        #
        rss = FeedParser.new(:url => 'https://news.yandex.ru/index.rss')

        @news = rss.parse.items.take(3)


        @main_product = Product.find(Setng::get("main_product"))
        @main_list_using = JSON.parse(Setng::get("main_list_using"))
        @main_list_additional
        @main_list_order = Setng::get("main_list_order").split("|")
        @main_list_examples = JSON.parse(Setng::get("main_list_examples"))
        @main_items = []
        used_cat_ids = []
        used_adcat_ids = []
        used_product_ids = []

        @main_list_order.each do |item|

            unless @main_list_using.include? item
                next
            end

            tmp = item.split("/")
            type = tmp[0]
            id = tmp[1]

            if type == "cat"
                @main_items.push Category.find(id)
            elsif type == "product"
                @main_items.push Product.find(id)
            end
        end
    end

    def contacts
        @creds_link = Setng::get("creds_link")
    end
end
