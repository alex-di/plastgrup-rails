
$.widget "alex-di.coordinator",
    options:
        debug: false

    _log: ->
        args = [].slice.call arguments
        console.log.apply console, args if @options.debug

    _create: ->
        self = @
        @$i = $ self.element
        @$i.on "click", (e) ->
            self._click e, self

    _click: (e, self) ->

        o =
            x: e.offsetX || e.pageX - @$i[0].offsetLeft
            y: e.offsetY || e.pageY - @$i[0].offsetTop

        percents =
            x: o.x / @$i.width() * 100
            y: o.y / @$i.height() * 100


        console.log(o, percents, @$i.width(), @$i.height())
		# highlight = $ "<div>", {class: "coords_pointer"}
		# highlight.css
		# 	left: coords.x + "%"
		# 	top: coords.y + "%"
        #
		# $(".coords_img_container").append highlight

        @_trigger "onClick", e, percents
