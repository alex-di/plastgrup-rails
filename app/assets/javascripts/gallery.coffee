
$.widget "alex-di.gallery",

    options:
        value: 0
        prfx: "gallery"
        current: []
        empty_text: "Нет изображений"
        add_text: "Добавить"
        list_text: "Открыть галерею"
        btn_class: "btn btn-default"
        debug: true
        modal_id: "gallery-modal"
        form_id: "gallery-form"
        token: ""

    $modalContainer: null

    _log: ->
        args = [].slice.call arguments
        console.log.apply console, args

    _updateCurrent: ->

        self = @

        @$container.html ""

        @$el.val @options.current.map (el)-> return el.id

        for item in @options.current
            do (item) ->
                $image = $ "<div class='image-container pull-left'><img src='#{item.file.thumb.url}'><a href='#' class='remove-link'><i class='fa fa-times'></i></a><a href='#' class='edit-link'><i class='fa fa-pencil'></i></a></div>"

                self.$container.append $image
                $image.on "click", "a.remove-link", ->
                    self.options.current = self.options.current.filter (el) -> el.id != item.id

                    $image.animate {"width": 0}, ->
                        self._updateCurrent()


                $image.on "click", "a.edit-link", ->
                    self._editElement(item)

        unless @options.current.length
            @$container.append "<div class='#{@options.prfx}-empty'>#{@options.empty_text}</div>"

    _editElement: (item) ->
        self = @

        do (item) ->
            $.ajax
                url: "/admin/images/#{item.id}/edit"
                dataType: "html"
                success: (res) ->
                    $form = $ res
                    self.$modalContainer.html $form
                    self.$modalContainer.remodal().open()
                    $form.ajaxForm (res) ->
                        self.$modalContainer.remodal().close()
                        el = self.options.current.find (e) -> e.id == item.id
                        self._updateCurrent()


    _create: ->
        self = @
        @$el = $ this.element

        @$el.wrap "<div class='clearfix'>"
        @$el.hide()

        @$container = $ "<div>",
            class: "#{@options.prfx}-preview-container clearfix"

        @_updateCurrent()

        @$addLink = $ "<div class='#{@options.prfx}-add-btn pull-left'><a href='#' data-remodal-target='#{@options.form_id}' class='#{@options.btn_class}'>#{@options.add_text}</a></div>"
        @$listLink = $ "<div class='#{@options.prfx}-list-btn pull-left'><a href='#' data-remodal-target='#{@options.modal_id}' class='#{@options.btn_class}'>#{@options.list_text}</a></div>"

        @$el.after @$container
        @$container.after @$addLink, @$listLink

        @$modalContainer = $ "<div>",
            class: "remodal gallery-preview-container "
            "data-remodal-id": @options.modal_id
            "data-remodal-options": "hashTracking:false"

        $("body").append @$modalContainer

        @$listLink.on "click", 'a', ->
            self._renderList()

        @$addLink.on "click", 'a', ->
            self._renderForm()

    _renderForm: ->
        self = @
        @$form = $ ".image-form form"
        @$form.ajaxForm (res) ->
            self._log("?", res)

            if(res.errors)
                console.log(res.errors)
                return

            self.$el.append "<option value='#{res.id}' selected=selected></option>"
            self.options.current.push res
            self._updateCurrent()
            self.$form.parents(".remodal").remodal().close()

    _renderList: ->

        self = @

        @_log @$modalContainer
        @$modalContainer.html ''

        $.get "/admin/images.json", (res) ->
            for item in res
                do (item) ->

                    $link = $ "<a href=# class='image-container pull-left'><img src='#{item.file.thumb.url}'></a>"
                    self.$modalContainer.append $link

                    self._log(item.id)
                    $link.on "click", ->
                        self.$modalContainer.remodal().close()

                        exists = self.options.current.find (i) -> i.id == item.id
                        if exists
                            alert("Изображение уже добавлено")
                            return

                        self._log item.id
                        self.options.current.push(item)
                        self._updateCurrent()

            unless res.length
                self.$modalContainer.append self.options.empty_text
