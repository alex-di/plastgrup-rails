json.array!(@dealers) do |dealer|
  json.extract! dealer, :id, :name, :city, :email, :phone, :address
  json.url dealer_url(dealer, format: :json)
end
