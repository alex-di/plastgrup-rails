module ApplicationHelper

    def preview (text, length = 300)
        truncate_html(sanitize(text, tags: ["br", "p"]).gsub(/\<p.*?\>(.*?)\<\/p\>/, '\1<br>') , length: length)
    end
end
