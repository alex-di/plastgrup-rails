class AddSearchTagsToProduct < ActiveRecord::Migration
  def change
    add_column :products, :search_tags, :text
  end
end
