class AddMetaToProduct < ActiveRecord::Migration
  def change
    add_column :products, :meta_title, :string
    add_column :products, :meta_description, :text
    add_column :products, :meta_keywords, :text
  end
end
