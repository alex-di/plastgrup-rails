class AddImagableToGallery < ActiveRecord::Migration
  def change
    add_reference :galleries, :imagable,  polymorphic: true
  end
end
