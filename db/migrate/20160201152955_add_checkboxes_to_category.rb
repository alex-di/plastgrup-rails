class AddCheckboxesToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :active, :boolean
    add_column :categories, :visible, :boolean
  end
end
