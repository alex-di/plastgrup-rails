class CreateRecommendations < ActiveRecord::Migration
  def change
    create_table :recommendations do |t|
      t.string :company
      t.string :city
      t.text :file

      t.timestamps null: false
    end
  end
end
