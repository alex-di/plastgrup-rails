class AddFieldsToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :image, :string
    add_column :properties, :subtitle, :string
  end
end
