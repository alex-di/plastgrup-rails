class AddTextPreviewToImage < ActiveRecord::Migration
  def change
    add_column :images, :text_preview, :boolean
  end
end
