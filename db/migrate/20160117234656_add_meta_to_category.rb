class AddMetaToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :meta_title, :string
    add_column :categories, :meta_description, :text
    add_column :categories, :meta_keywords, :text
  end
end
