class AddEmailToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :email, :string
  end
end
