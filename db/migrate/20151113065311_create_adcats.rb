class CreateAdcats < ActiveRecord::Migration
  def change
    create_table :adcats do |t|
      t.string :name
      t.text :html

      t.timestamps null: false
    end
  end
end
