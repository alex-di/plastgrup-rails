class AddPositionToFactoid < ActiveRecord::Migration
  def change
    add_column :factoids, :position, :number
  end
end
