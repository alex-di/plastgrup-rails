class AddFieldsToProduct < ActiveRecord::Migration
  def change
    add_reference :products, :category, index: true, foreign_key: true
    add_column :products, :name, :string
  end
end
