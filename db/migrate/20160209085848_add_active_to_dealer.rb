class AddActiveToDealer < ActiveRecord::Migration
  def change
    add_column :dealers, :active, :boolean
  end
end
