class CreateProperties < ActiveRecord::Migration
  def change
    create_table :properties do |t|
      t.string :title
      t.text :values
      t.references :product, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
