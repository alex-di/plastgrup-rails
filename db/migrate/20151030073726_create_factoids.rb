class CreateFactoids < ActiveRecord::Migration
  def change
    create_table :factoids do |t|

      t.timestamps null: false
      t.string :title
      t.references :product
      t.string :type
      t.text :description
    end
  end
end
