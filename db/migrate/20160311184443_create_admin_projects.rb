class CreateAdminProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.text :logo
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
