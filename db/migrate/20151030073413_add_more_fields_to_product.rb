class AddMoreFieldsToProduct < ActiveRecord::Migration
  def change
    add_column :products, :slogan, :string
    add_column :products, :image, :string
    add_column :products, :subtitle, :string
    add_column :products, :description, :text
    add_column :products, :complect_image, :string
    add_column :products, :questionare_file, :string
  end
end
