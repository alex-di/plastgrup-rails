class AddFieldsToFactoid < ActiveRecord::Migration
  def change
    add_reference :factoids, :client, index: true, foreign_key: true
  end
end
