class RemoveDisplayFromGallery < ActiveRecord::Migration
  def change
    remove_column :galleries, :display, :boolean
  end
end
