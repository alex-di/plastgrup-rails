class AddFileToFactoid < ActiveRecord::Migration
  def change
    add_column :factoids, :file, :text
  end
end
