class AddPropertiesToProduct < ActiveRecord::Migration
  def change
    add_column :products, :properties_unit, :string
    add_column :products, :properties_options, :text
  end
end
