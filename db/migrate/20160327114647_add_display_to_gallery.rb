class AddDisplayToGallery < ActiveRecord::Migration
  def change
    add_column :galleries, :display, :boolean
  end
end
