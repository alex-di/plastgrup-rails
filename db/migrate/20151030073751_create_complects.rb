class CreateComplects < ActiveRecord::Migration
  def change
    create_table :complects do |t|

      t.timestamps null: false

      t.string :title
      t.text :description
      t.string :type
      t.references :product
    end
  end
end
