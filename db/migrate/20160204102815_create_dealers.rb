class CreateDealers < ActiveRecord::Migration
  def change
    create_table :dealers do |t|
      t.string :name
      t.string :city
      t.string :email
      t.string :phone
      t.string :address

      t.timestamps null: false
    end
  end
end
