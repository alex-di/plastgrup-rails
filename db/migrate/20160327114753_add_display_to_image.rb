class AddDisplayToImage < ActiveRecord::Migration
  def change
    add_column :images, :display, :boolean
  end
end
