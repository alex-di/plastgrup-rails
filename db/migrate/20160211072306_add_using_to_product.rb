class AddUsingToProduct < ActiveRecord::Migration
  def change
    add_column :products, :using, :boolean
  end
end
