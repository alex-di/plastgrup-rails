class AddSearchTagsToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :search_tags, :text
  end
end
