# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160414085301) do

  create_table "adcats", force: :cascade do |t|
    t.string   "name"
    t.text     "html"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "attachments", force: :cascade do |t|
    t.string   "group"
    t.string   "type"
    t.string   "name"
    t.string   "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "product_id"
  end

  add_index "attachments", ["product_id"], name: "index_attachments_on_product_id"

  create_table "categories", force: :cascade do |t|
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "name"
    t.text     "description"
    t.string   "image"
    t.string   "meta_title"
    t.text     "meta_description"
    t.text     "meta_keywords"
    t.string   "slug"
    t.text     "search_tags"
    t.boolean  "active"
    t.boolean  "visible"
    t.string   "email"
  end

  add_index "categories", ["slug"], name: "index_categories_on_slug", unique: true

  create_table "categories_products", id: false, force: :cascade do |t|
    t.integer "category_id"
    t.integer "product_id"
  end

  add_index "categories_products", ["category_id"], name: "index_categories_products_on_category_id"
  add_index "categories_products", ["product_id"], name: "index_categories_products_on_product_id"

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "image"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "complects", force: :cascade do |t|
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "title"
    t.text     "description"
    t.string   "type"
    t.integer  "product_id"
    t.string   "feature"
    t.text     "coords"
  end

  create_table "dealers", force: :cascade do |t|
    t.string   "name"
    t.string   "city"
    t.string   "email"
    t.string   "phone"
    t.string   "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean  "active"
  end

  create_table "factoids", force: :cascade do |t|
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "title"
    t.integer  "product_id"
    t.string   "type"
    t.text     "description"
    t.integer  "client_id"
    t.decimal  "position"
    t.text     "file"
  end

  add_index "factoids", ["client_id"], name: "index_factoids_on_client_id"

  create_table "faqs", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"

  create_table "galleries", force: :cascade do |t|
    t.integer  "image_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "imagable_id"
    t.string   "imagable_type"
  end

  add_index "galleries", ["image_id"], name: "index_galleries_on_image_id"

  create_table "images", force: :cascade do |t|
    t.integer  "gallery_id"
    t.string   "gallery_type"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "file"
    t.text     "text"
    t.boolean  "text_preview"
    t.boolean  "display"
  end

  create_table "products", force: :cascade do |t|
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "name"
    t.string   "slogan"
    t.string   "subtitle"
    t.text     "description"
    t.string   "complect_image"
    t.string   "questionare_file"
    t.string   "properties_unit"
    t.text     "properties_options"
    t.boolean  "active"
    t.boolean  "visible"
    t.string   "meta_title"
    t.text     "meta_description"
    t.text     "meta_keywords"
    t.string   "listname"
    t.string   "slug"
    t.text     "search_tags"
    t.text     "guarantee"
    t.boolean  "using"
    t.text     "coords"
    t.string   "email"
  end

  add_index "products", ["slug"], name: "index_products_on_slug", unique: true

  create_table "projects", force: :cascade do |t|
    t.text     "logo"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "properties", force: :cascade do |t|
    t.string   "title"
    t.text     "values"
    t.integer  "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "image"
    t.string   "subtitle"
    t.string   "option"
    t.text     "dwg"
    t.text     "pdf"
  end

  add_index "properties", ["product_id"], name: "index_properties_on_product_id"

  create_table "recommendations", force: :cascade do |t|
    t.string   "company"
    t.string   "city"
    t.text     "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "password_digest"
    t.string   "login"
  end

end
