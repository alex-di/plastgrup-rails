# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(login: "admin", password: "admin")

Category.create(name: "Ливневая канализация")

Product.create(
	name: "Маслобензоотделитель", 
	slogan: "Производим в Санкт-Петербурге, доставляем по России и СНГ",
	subtitle: "Маслобензоотделители серии «МБ»",
	description: "Очищают ливневые стоки от легких жидкостей и нефтепродуктов перед выводом в канализацию. Комплектуем встроенным пескоуловителем для очистки от взвеси в небольших системах очистки."
	)

Factoid.create(
	title: "12 лет без брака",
	description: "12 ЛЕТ, КАРЛ!",
	product_id: 1
	)