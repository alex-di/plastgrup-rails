# README #

После установки rails выполнить следующее

* bundle install
* rake db:migrate 
* rake db:seed

Сервер запускается командой rails server 

Открывается по адресу localhost:3000

Базу запускать не нужно, используется sqlite

Темлпейты все лежат в app/views, app/views/admin - подпапка исплючительно для темплейтов админки. 
Основной layout соответственно app/views/layouts/application.html.slim