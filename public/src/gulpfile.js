var gulp = require('gulp');
var stylus = require('gulp-stylus');
var prefix  = require('gulp-autoprefixer');
var rupture = require('rupture');
var uglify = require('gulp-uglify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserify = require('browserify');
var babel = require('babelify');


gulp.task('styles', function () {
  gulp.src('./stylus/*.styl')
  .pipe(stylus({
    compress:  true,
    use: rupture(),
  }))
  .on('error', function (err) {
    console.error('Error!', err.message);
  })
  .pipe(prefix('last 3 versions'))
  .pipe(gulp.dest('../css'));
});

gulp.task('scripts', function () {
  var bundler = browserify('./scripts/main.js', { debug: true }).transform(babel);
  return bundler
    .bundle()
    .on('error', function(err) { console.error(err); this.emit('end'); })
    .pipe(source('build.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest('../js'));
});

gulp.task('default', ['styles', 'scripts'], function () {
  gulp.watch('./stylus/**/*.styl', ['styles']);
  gulp.watch('./scripts/**/*.js', ['scripts']);
});
