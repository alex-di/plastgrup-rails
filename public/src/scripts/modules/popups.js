export default function () {
	$('.contacts-form__input.stretchy').text(' ');
	setTimeout(() => $('.new-popup').addClass('new-popup--initialized'), 500);

	$('.contacts-form__input.stretchy').text('');
	$('.new-popup').on('click', function(event) {
		var target = event.target;
		var currentTarget = event.currentTarget;

		if (target.classList.contains('new-popup__inner') || target.classList.contains('new-popup__close')) {
			$(currentTarget).fadeOut();
		}
	});

	subscribePopup();

}

function subscribePopup() {
	var validation = {
		$popup: $('.subscribe-popup .new-popup__inner'),
		$form: $('.subscribe-popup .new-popup__window'),
		$front: $('.subscribe-popup .new-popup__side--front'),
		$back: $('.subscribe-popup .new-popup__side--back'),
		$email: $('#subscribeemail'),
		$btn: $('.subscribe-popup .contacts-form__btn'),
		$annotation: $('.subscribe-popup__annotation'),

		emailValid: function(value) {
			var emailRegexp = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			return emailRegexp.test(value);
		},

		init: function() {
			this.$form.on('submit', event => {
				event.preventDefault();

				const val = this.$email.val();

				if (val === '' || !this.emailValid(val)) {
					this.$email.addClass('contacts-form__input--invalid');
					this.$annotation.addClass('subscribe-popup__annotation--warning');
					this.$annotation.text('Проверьте адрес эл. почты.');
				} else {
					this.$popup.addClass('new-popup--flipped')
				}
			});

			this.$email.on('keyup keypress change input', () => {
				const val = this.$email.val();

				if (this.emailValid(val)) {
					this.$btn.removeAttr('data-disabled');
					this.$annotation.removeClass('subscribe-popup__annotation--warning');
					this.$annotation.text('Без спама');
				} else {
					this.$btn.attr('data-disabled', 'true');
				}
			});
		}
	};

	validation.init();

	$('.main-news__btn a').on('click', function(event) {
		event.preventDefault();
		$('#subscribe-popup').fadeIn();
	});

	contactPopup();
}

function contactPopup() {
	$('[data-popup="contact"]').click(function () {
		$('#contact-popup').fadeIn();
	});

	$('.contacts-form__file').click((event) => {
		event.preventDefault();
		$(event.currentTarget).closest('.contacts-form').find('.contacts-form__file-input').click();
	});

	$('.contacts-form__file-input').change(function (event) {
		event.currentTarget.parentNode.querySelector('.contacts-form__file').innerText = event.target.files[0].name;
	});

	const validation = {
		emailOrPhoneValid: function(value) {
			const emailRegexp = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			let isValid = false;
			
			if (!emailRegexp.test(value)) {
				const valueNumbersCount = value.match(/\d/g) || [];
				isValid = valueNumbersCount.length >= 11;
			} else {
				isValid = true;
			}

			return isValid;
		},

		init: function() {
			$('.contacts-form').on('submit', (event) => {
				event.preventDefault();
				const activeErrorClass = 'contacts-form__error-annotation--active';
				const $phoneAndEmail = $(event.currentTarget.querySelector('.contacts-form__input--phone-email'));
				const emptyError = event.currentTarget.querySelector('.contacts-form__error-annotation--empty');
				const phoneError = event.currentTarget.querySelector('.contacts-form__error-annotation--phone');
				const value = $phoneAndEmail.val();
				const isValid = this.emailOrPhoneValid(value);
				const valueNumbersCount = value.match(/\d/g) || [];

				if (value.trim() === '' || !isValid) {
					$phoneAndEmail.addClass('contacts-form__input--invalid');
				}

				if (value.trim() === '') {
					phoneError.classList.remove(activeErrorClass);
					emptyError.classList.add(activeErrorClass);
				} else if (!isValid && value.indexOf('@') === -1 && valueNumbersCount.length > 0) {
					emptyError.classList.remove(activeErrorClass);
					phoneError.classList.add(activeErrorClass);
				}

				if (value.trim() !== '' && isValid) {
					$(event.currentTarget).closest('.new-popup').addClass('new-popup--flipped');
				}
			});

			$('.contacts-form__input--phone-email').on('input', (event) => {
				const activeErrorClass = 'contacts-form__error-annotation--active';
				const $currentTarget = $(event.currentTarget);
				const value = $currentTarget.val();

				const $btn = $currentTarget.closest('.contacts-form').find('.contacts-form__btn');

				if (this.emailOrPhoneValid(value)) {
					const activeError = event.currentTarget.parentNode.querySelector(`.${activeErrorClass}`);

					$btn.removeAttr('data-disabled');
					activeError && activeError.classList.remove(activeErrorClass);
					event.currentTarget.classList.remove('contacts-form__input--invalid');
				} else {
					$btn.attr('data-disabled', 'true');
				}
			});
		},
	};

	validation.init();
}
