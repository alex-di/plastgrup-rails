$(function(){
  
  var ios = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
  if (ios) $('html').addClass('ios');
  
  var Filter = function(config) {
    
    var $searchField,
      $items,
      $visibleItems,
      $tags,
      $notif,
      $examples,
      $allExamples;
  
    this.config = config || {};
    
    this.init = function() {

      if (!config.search || !$(config.search).length) {
        console.error('Filter: no search field defined');
        return;
      }
      
      if (!config.items || !$(config.items).length) {
        console.error('Filter: no filter items defined');
        return;
      }
      
      if (!config.tags || !$(config.tags).length) {
        console.error('Filter: no tag items defined');
        return;
      }
      
      if (!config.notif || !$(config.notif).length) {
        console.error('Filter: no notification items defined');
        return;
      }
      
      if (!config.examples || !$(config.examples).length) {
        console.error('Filter: no examples items defined');
        return;
      }
      
      if (!config.allExamples || !$(config.allExamples).length) {
        console.info('Filter: no allExamples items defined');
      }
      
      $searchField = $(config.search);
      $items = $(config.items);
      $tags = $(config.tags);
      $notif = $(config.notif);
      $examples = $(config.examples);
      $allExamples = $(config.allExamples);
      
      $searchField.parents('form').on('submit', function(e){
        e.preventDefault();
        return false;
      });
      
      this.eventHandlers();
    };
    
    
    this.eventHandlers = function(){
      
      var self = this;
      
      $searchField.on('focus', function(){
        var _this = this;
        self.checkFieldWidth();
        if ($(_this).val().toLowerCase() == 'продукты') $(_this).val('');
        
        setTimeout(function(){
          _this.setSelectionRange(0, _this.value.length)
          _this.select();
        
          self.checkFieldWidth();
        }, 210);
        
      });
      
      $searchField.on('change keyup focus', function(){
        self.checkFieldWidth();
        self.filterItemsByQuery($(this).val());
      });
      
      $searchField.on('change keyup', function(){
        self.changeExamples($(this).val());
      });
      
      $searchField.on('blur', function(){
        var val = $(this).val();
        if (val == '') $(this).val('Продукты');
        
        self.checkFieldWidth();
      });
      
      $tags.on('click', function(e){
        e.preventDefault();
        self.setTag(e);
        self.checkFieldWidth();
        return false;
      });
      
      $allExamples .on('click', function(e){
        e.preventDefault();
        self.showExamples();
        return false;
      });
      
    };
    
    
    this.checkFieldWidth = function() {
      
      $('#search-hidden').html($searchField.val());
      
      var targetWidth = $('#search-hidden').width()+182,
        currentWidth = $searchField.width(),
        self = this;

      if (targetWidth > 560) {
        $searchField.css({'max-width':targetWidth+'px', 'width':targetWidth+'px'});
      }
      else {
        self.removeFieldWidth();
      }
      
    };
    

    this.removeFieldWidth = function() {
      $searchField.removeAttr('style');
    };
    
    
    this.changeExamples = function(val) {
      val = val.toLowerCase();
      if (val !== '' && val !== 'продукты') this.hideExamples();
      else this.showExamples();
    };
    
    
    this.hideExamples = function() {
      $examples.hide();
      $allExamples.css({'display': 'block'});
    };
    
    
    this.showExamples = function() {
      $examples.show();
      $allExamples.hide();
      this.filterItemsByQuery('');
      $searchField.val('').blur();
    };
    

    this.filterItemsByQuery = function(query){
      
      var self = this;
      self.hideNotificationOfNoResult();
      
      $items.each(function(){
        var $this = $(this),
          filter = $this.attr('data-filter').toLowerCase().replace('ё','е'),
          $subFilter = $this.find('[data-subfilter]');
        
        query = query.toLowerCase().replace('ё','е');
        
        // если пустое поле или фокус-расфокус
        if (query == '' || query == 'продукты') {
          
          $this.show();
          $subFilter.show();
          $searchField.removeClass('active');
          
        }
        // фильтруем
        else {
          
          $searchField.addClass('active');
          
          if (filter.indexOf(query) > -1) {
            $this.show();
          }
          else {
            $this.hide();
          }
          
          // фильтруем подфильтры
          $subFilter.each(function(){
            var $this = $(this),
              subfilter = $this.attr('data-subfilter').toLowerCase().replace('ё','е');
            
            if (subfilter.indexOf(query) > -1) {
              $this.parents('[data-filter]').show();
              $this.show();
            }
            else {
              $this.hide();
            }
          });
          
          // показываем всё, если подфильтров нет
          if (!$this.find('[data-subfilter]:visible').length) {
            $subFilter.show();
          }
          
        }

      });
      
      $visibleItems = $(config.items+':visible');
      if (!$visibleItems.length) self.showNotificationOfNoResult();

      
    };
    
    
    this.showNotificationOfNoResult = function() {
      $notif.show();
    };
    

    this.hideNotificationOfNoResult = function() {
      $notif.hide();
    };
    
    
    this.setTag = function(e) {
      
      var $tag = $(e.target).closest('.tag'),
        text = $tag.text();
      
      $searchField.val(text).trigger('change');
      //this.showExamples();
      
    };
    

    this.init();
    
    return this;
    
  };
  
  
  
  var filter = new Filter({
    search: '#search-field',
    items: '[data-filter]',
    tags: '.tag',
    notif: '.search-notification',
    examples: '.search-field__esamples',
    allExamples: '.search-field__esamples--hidden'
  });
  
  
});