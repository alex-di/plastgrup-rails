$(window).load(function() {
  var fileHoverIn = function() {
    $(this).addClass('hover');
    if ($(this).hasClass('application-form__file')) {
      $(this).next().addClass('hover');
    } else {
      $(this).prev().addClass('hover');
    }
  };
  var fileHoverOut = function() {
    $(this).removeClass('hover');
    if ($(this).hasClass('application-form__file')) {
      $(this).next().removeClass('hover');
    } else {
      $(this).prev().removeClass('hover');
    }
  };

  var fileClick = function(e) {
    e.preventDefault();
    $('.application-form--hidden .application-form__file-input').click();
  };

  $('.application-form__file, .application-form__icon').hover(fileHoverIn, fileHoverOut);
  $('.application-form__file, .application-form__icon').click(fileClick);

  // обрабатываем выбор файла
  $('.application-form--hidden .application-form__file-input').change(function(e) {
    $('.application-form__file').text(e.target.files[0].name);
  });

  $('.application-form__textarea').on('keyup change paste', function() {
    $(this).attr('data-this', 'true');
    $('.application-form__textarea:not([data-this])').val($(this).val());
    $(this).removeAttr('data-this');
  });

  $('.application-form__tel-input').on('keyup change change paste', function() {
    $(this).attr('data-this', 'true');
    $('.application-form__tel-input:not([data-this])').val($(this).val());
    $(this).removeAttr('data-this');
  });

  $('.application-form__btn').click(function(e) {
    e.preventDefault();
    $('.application-form--hidden form').submit();
  });


  // textarea
  var initialHeight = $('.application-form--stable .application-form__textarea').height();

  $('.application-form__textarea').on('change keyup mouseup propertychange paste', function() {
    var $this = $(this);
    var val = $this.val();
    var keyLength = 12, lineLength = $this.width();
    
    if (val.length > 0) {
      $('.application-form__file').fadeOut(150, function() {
        $this.addClass('not-empty');
      });
      var phraseLength = Math.ceil( val.length * keyLength / lineLength ) + (val.split('\n').length - 1);

      $this.height(phraseLength * initialHeight);
    } else {
      $this.height(initialHeight);
      $this.removeClass('not-empty');
      $('.application-form__file').fadeIn(150);
    }
  });

  $('.application-form__textarea').focus(function() {
    var $this = $(this);
    if ($this.val().length > 0) {
      $('.application-form__file').fadeOut(150, function() {
        $this.parent().addClass('focused');
        $this.addClass('not-empty');
      });
    } else {
      $this.parent().addClass('focused');
      $this.removeClass('not-empty');
    }
  });

  $('.application-form__textarea').blur(function() {
    var $this = $(this);
    $this.parent().removeClass('focused');
    $this.height(initialHeight);
    $this.removeClass('not-empty');

    $('.application-form__file').fadeIn(150);
  });


  // переливающийся фон
  var smoothBg = {
    speed: 3000,

    layers: [
      $('.main-page__bg .layer--1'),
      $('.main-page__bg .layer--2'),
      $('.main-page__bg .layer--3')
    ],

    in: function() {
      var self = this;

      self.layers[0].fadeOut(self.speed);
      self.layers[1].fadeIn(self.speed, function() {
        self.layers[1].fadeOut(self.speed);
        self.layers[2].fadeIn(self.speed, function() {
          self.out();
        })
      });
    },

    out: function() {
      var self = this;

      self.layers[2].fadeOut(self.speed);
      self.layers[1].fadeIn(self.speed, function() {
        self.layers[1].fadeOut(self.speed);
        self.layers[0].fadeIn(self.speed, function() {
          console.log('in!');
          self.in();
        });
      });
    },

    init: function() {
      this.out();
    }
  };

  smoothBg.init();


  // карусель на альтернативной главной
  $('.main-alternate__slider').owlCarousel && $('.main-alternate__slider').owlCarousel({
    singleItem: true,

    navigation: true,
    pagination: false,
    autoHeight: true,

    mouseDrag: false,

    transitionStyle: 'fade'
  });
});