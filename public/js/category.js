
$(function() {



    	// ! popup

    	function popup() {

    		var $popup = $('.popup');
    		if (!$popup.length) {return false;}

    		var timer = 400,
    				ampl = 150,
    				delta = 50;

    		function showOverlay() {
    			$('.overlay').fadeIn(timer);
    			$('html').addClass('noscroll');
    		};

    		function hideOverlay() {
    			$('.overlay').fadeOut(timer)
    			$('html').removeClass('noscroll');
    		};

    		function openPopup(popid) {
    			var $p = $('#'+popid);
    			var scr = $(window).scrollTop() + delta;

    			var $iframe = $('#list-frame'),
    					src = 'http://www.plastgrup.ru/questionnaire/pump-station';

    			showOverlay();
    			$p.fadeIn(timer);
    		};

    		function closePopup(popid) {
    			var $p = $('#'+popid);
    			hideOverlay();
    			$p.fadeOut(timer);
    		};

    		function closePopups() {
    			hideOverlay();
    			$popup.fadeOut(timer);
    		};


    		$('#gost-popup, .gostcol').on('click', function(e){
    			e.preventDefault();
    			openPopup('popup1');
    		});

    		$('#requirements-popup').on('click', function(e){
    			e.preventDefault();
    			openPopup('popup2');
    		});

    		// photo gallery
    		$('html').on('click', function(e){
    			if ($(e.target).closest('.photo-gallery-trigger').length) {
    				e.preventDefault();
    				openPopup('popup-gallery');
    				GalleryEvents.openElementIdx = $(e.target).closest('[data-gallery-idx]').attr('data-gallery-idx') ||
    					$(e.target).find('[data-gallery-idx]').attr('data-gallery-idx');
    				$(GalleryEvents).trigger('gallery-open');
    			}
    		});

    		$('.popup-close, .overlay, .popup-hide').on('click', function(e){
    			e.preventDefault();
    			closePopups();
    		});


    	};

    	popup();

        /* popup photo gallery */

		var GalleryEvents = {};

		function initPhotoGallery() {

			var $popup = $('#popup-gallery'),
				$images = $('[data-gallery-popup]'),
				$header = $('#photo-gallery-title'),
				template = 'Фотографии {1}, {2} и {3}',
				itemTemplate = '<a href="#" class="photo-gallery-link" data-gallery-link-idx="{groupIdx}">{item}</a>';
				id = 0;

			if (!$images.length || !$popup.length) {return false;}

			$images.each(function(){
				var $image = $(this).clone(),
					group = parseInt($image.attr('data-gallery-group')),
					groupName = $image.attr('data-gallery-group-name'),
					caption = $image.attr('data-gallery-caption') || '';

				$(this).attr('data-gallery-idx', id).addClass('photo-gallery-trigger');
				$image.attr('data-gallery-idx', id).removeAttr('style').addClass('photo-gallery-image');
				id++;

				if (!$popup.find('.gallery-group--'+group).length) {
					$popup.find('.popup-self').append('<div class="gallery-group gallery-group--'+group+'" data-gallery-group-idx="'+group+'"></div>');
					template = template.replace('{'+group+'}', itemTemplate.replace('{item}', groupName).replace('{groupIdx}', group));
				}
				$popup.find('.gallery-group--'+group).append('<div class="gallery-item"></div>');
				$popup.find('.gallery-group--'+group+' .gallery-item:last').append($image).append('<p>'+caption+'</p>');
			});

			template = template.replace('{1}', '').replace(', {2}', '').replace('и {3}', '');
			$header.html(template);

			$('html').find('.photo-gallery-link:first').addClass('active');


			var $links = $('html').find('.photo-gallery-link');
			$links.on('click', function(e){
				e.preventDefault();

				var idx = $(this).attr('data-gallery-link-idx'),
					offt = $popup.find('.gallery-group--'+idx).offset().top - $popup.offset().top + $popup.scrollTop() - 80;

				$popup.animate({scrollTop: offt+'px'}, TIMER);
			});


			$popup.scroll(function(){
				var sct = $(this).scrollTop();

				$popup.find('.gallery-group').each(function(){
					var $this = $(this),
						limit = $popup.offset().top + $popup.scrollTop(),
						offt = $this.offset().top + $popup.offset().top,
						offtAndH = offt + $this.outerHeight();

					//console.log($(this).attr('class'), offt, offtAndH, limit);

					if (offt < limit && offtAndH > limit) {
						var idx = parseInt($(this).attr('data-gallery-group-idx'));
						$links.removeClass('active');
						$('[data-gallery-link-idx="'+idx+'"]').addClass('active');
					}
				});
			});


			$(GalleryEvents).on('gallery-open', function(){
				$popup.scrollTop(0);

				var idx = GalleryEvents.openElementIdx,
					offt = $popup.find('[data-gallery-idx="'+idx+'"]').offset().top - $popup.offset().top - 80;

				$popup.scrollTop(offt)
			});

		};

		initPhotoGallery();


  // координаты точек на изображении
  var points = [
    [588, 350],
    [670, 402],
    [548, 160],
    [385, 351],
    [726, 295],
    [226, 68]
  ];

  // настраиваем окружение
  var initRaphael = function() {
    var image, $svg, $baseContainer, $img, paper, paperCoords, paperSize;
    var onWindowResize, onLinkHoverIn, onLinkHoverOut, draw;

    $baseContainer = $('.category-products__inner');
    $img = $('.category-products__image img');

    paperCoords = [$baseContainer.offset().left, $baseContainer.offset().top];
    paperSize = [$baseContainer.innerWidth(), $baseContainer.innerHeight()];

    // описываем наше изображение
    image = {
      width: $img.width(),
      height: $img.height(),
      position: {
        x: $img.offset().left - $baseContainer.offset().left,
        y: $img.offset().top - $baseContainer.offset().top
      }
    };

    // создаём бумагу
    paper = Raphael(paperCoords[0], paperCoords[1], paperSize[0], paperSize[1]);
    $svg = $('body').children('svg');

    // чтобы наша «бумага» не перекрывала ссылки
    $svg.css({
      'z-index': 50
    });

    // при наведении мыши на ссылку
    onLinkHoverIn = function(e) {
      var dot = $(this).children('.dot');

      dotCoords = [dot.offset().left - $baseContainer.offset().left, dot.offset().top - $baseContainer.offset().top, ];
      draw(e.target.getAttribute('data-point'), dotCoords);
    };

    onLinkHoverOut = function() {
      paper.clear();
    };

    draw = function(point, dot) {
      var p = points[point - 1];
      var pointCoords = [image.position.x + p[0], image.position.y + p[1]];

      var circle1 = paper.circle(pointCoords[0], pointCoords[1], 2.5);
      var circle2 = paper.circle(dot[0], dot[1], 2.5);
      var line = paper.path("M" + dot[0] + " " + dot[1] + "L" + pointCoords[0] + " " + pointCoords[1]);

      circle1.attr("fill", "#b00");
      circle1.attr("stroke-width", 0);
      circle2.attr("fill", "#b00");
      circle2.attr("stroke-width", 0);
      line.attr("stroke", "#b00");
    };

    onWindowResize = function() {
      var newWidth = $baseContainer.innerWidth(), newHeight = $baseContainer.innerHeight();

      image.position = {
        x: $img.offset().left - $baseContainer.offset().left,
        y: $img.offset().top - $baseContainer.offset().top
      };

      $svg.css({
        top: $baseContainer.offset().top,
        left: $baseContainer.offset().left,
        width: newWidth,
        height: newHeight
      });

      paper.setViewBox(0, 0, newWidth, newHeight);
    };

    $(window).resize(onWindowResize);
    $('.category-products__link a').hover(onLinkHoverIn, onLinkHoverOut);
  };

  // инициализируем рафаэльку
  if($('.category-products__image img').length){
      $('.category-products__image img').one('load', function() {
        initRaphael();
      }).each(function() {
        if (this.complete) $(this).trigger('load');
      });

      initRaphael();
  }


  // обрабатываем клики и ховеры для формы прикрепления файла
  var fileHoverIn = function() {
    $(this).addClass('hover');
    if ($(this).hasClass('application-form__file')) {
      $(this).next().addClass('hover');
    } else {
      $(this).prev().addClass('hover');
    }
  };
  var fileHoverOut = function() {
    $(this).removeClass('hover');
    if ($(this).hasClass('application-form__file')) {
      $(this).next().removeClass('hover');
    } else {
      $(this).prev().removeClass('hover');
    }
  };

  var fileClick = function(e) {
    e.preventDefault();
    $('.application-form--hidden .application-form__file-input').click();
  };

  $('.application-form__file, .application-form__icon').hover(fileHoverIn, fileHoverOut);
  $('.application-form__file, .application-form__icon').click(fileClick);

  // обрабатываем выбор файла
  $('.application-form--hidden .application-form__file-input').change(function(e) {
    $('.application-form__file').text(e.target.files[0].name);
  });

  // синхронизация форм
  $('.application-form__textarea').on('keyup change paste', function() {
    $(this).attr('data-this', 'true');
    $('.application-form__textarea:not([data-this])').val($(this).val());
    $(this).removeAttr('data-this');
  });

  $('.application-form__tel-input').on('keyup change change paste', function() {
    $(this).attr('data-this', 'true');
    $('.application-form__tel-input:not([data-this])').val($(this).val());
    $(this).removeAttr('data-this');
  });

  $('.application-form__btn').click(function(e) {
    e.preventDefault();
    $('.application-form--hidden form').submit();
  });

  var initialHeight = $('.application-form--stable .application-form__textarea').height();
  // ввод в textarea
  $('.application-form__textarea').on('change keyup mouseup propertychange paste', function() {
    var $this = $(this);
    var val = $this.val();
    var keyLength = 12, lineLength = $this.width();

    if (val.length > 0) {
      $('.application-form__file').fadeOut(150, function() {
        $this.addClass('not-empty');
      });
      var phraseLength = Math.ceil( val.length * keyLength / lineLength ) + (val.split('\n').length - 1);

      $this.height(phraseLength * initialHeight);
    } else {
      $this.height(initialHeight);
      $this.removeClass('not-empty');
      $('.application-form__file').fadeIn(150);
    }
  });

  $('.application-form__textarea').focus(function() {
    var $this = $(this);
    if ($this.val().length > 0) {
      $('.application-form__file').fadeOut(150, function() {
        $this.parent().addClass('focused');
        $this.addClass('not-empty');
      });
    } else {
      $this.parent().addClass('focused');
      $this.removeClass('not-empty');
    }
  });

  $('.application-form__textarea').blur(function() {
    var $this = $(this);
    $this.parent().removeClass('focused');
    $this.height(initialHeight);
    $this.removeClass('not-empty');

    $('.application-form__file').fadeIn(150);
  });

  // обрабатываем скролл до формы
  var initPinnedForm = function() {
    var $pinnedForm = $('.application-form--pinned');
    var pinnedFormHeight = $pinnedForm.height();
    var pinnedFormInnerHeight = $pinnedForm.innerHeight();


    var limit = $('.application-form--stable').offset().top - $(window).height() + pinnedFormHeight * 0.8;

    var fadeForm = function(offsetTop) {
      $pinnedForm.css({
        opacity: Math.min(Math.max((1 - ((offsetTop - limit) / 75)), 0), 1)
      });
    };

    $(window).scroll(function(e) {
      var scrollTop = $(window).scrollTop();
    //   console.log(scrollTop);
      if (scrollTop > limit) {
        fadeForm(scrollTop);

        if (scrollTop >= limit + pinnedFormHeight) {
          $pinnedForm.removeAttr('style');
          $pinnedForm.hide();
          $('.application-form__textarea').blur();
        } else {
          $pinnedForm.show();
        }
      } else if (scrollTop < limit) {
        $pinnedForm.removeAttr('style');
        $pinnedForm.show();
      }
    });
  };

  $(window).load(function() {
    initPinnedForm();
  });


  // блок со ссылками на исследования, руководства, сертификаты и схемы
  var links = {
    $btn: $('a.shadow__text'),
    $list: $('.links-list__list'),
    maxHeight: 270,

    state: {
      collapsed: true
    },

    expand: function() {
      this.$btn.html('<span>Скрыть все документы</span>');

      $('.links-list__column').css({
        'max-height': '999999px',
        'overflow': 'initial'
      });

      this.state.collapsed = false;
    },

    collapse: function() {
      this.$btn.html('<span>Показать все документы</span>');

      $('.links-list__column').css({
        'max-height': this.maxHeight,
        'overflow': 'hidden'
      });

      this.state.collapsed = true;
    },

    init: function() {
      var self = this;

        if(self.$list.height() > 270){
            self.$btn.click(function(e) {
                e.preventDefault();
                (self.state.collapsed) ? self.expand() : self.collapse();
            });
        } else {
            self.$btn.parents(".shadow").remove()
        }
    }
  };

  links.init();
});
