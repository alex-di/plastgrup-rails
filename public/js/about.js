$(window).load(function() {
	$('.projects-inline__list').scroller();

	var $fileLink = $('.contacts-form__file'),
			$fileInput = $('#file-input');

	$fileLink.click(function(e) {
		e.preventDefault();

		$fileInput.click();
	});

	$fileInput.change(function() {
		$fileLink.text($(this)[0].files[0].name);
	});


	$('[data-modal]').on('click', function(event) {
		event.preventDefault();

		$('html').addClass('noscroll');

		$(event.currentTarget.getAttribute('data-modal'))
			.addClass('modal--active')
			.fadeIn(300);
	});

	$('.modal__close, .modal').on('click', function(event) {
		event.preventDefault();

		var t = event.target;

		if (!t.classList.contains('modal') && !t.classList.contains('modal__content') && !t.classList.contains('modal__close')) return;

		$('html').removeClass('noscroll');

		$('.modal--active')
			.fadeOut(150)
			.removeClass('modal--active');
	});

	$('.page-about').bookmark();
});
