require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get template1" do
    get :template1
    assert_response :success
  end

  test "should get template2" do
    get :template2
    assert_response :success
  end

end
