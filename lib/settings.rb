module Setng
  SETTINGS_FILENAME = "config/settings.yml"
  @settings = nil

  def self.load
    require 'yaml'

    @settings ||= nil
    unless File.exists? SETTINGS_FILENAME
      File.open(SETTINGS_FILENAME, 'w') { |file| file.write({}.to_yaml) }
    end
    new_settings_time = File.ctime(SETTINGS_FILENAME)
    if @settings.nil? || new_settings_time > @settings_time
      @settings_time = new_settings_time
      Rails.logger.info "Settings changed, reloading"
      settings_default ||= YAML.load_file 'config/settings.default.yml'
      begin
        settings_user ||= YAML.load_file SETTINGS_FILENAME
      rescue Exception => e
        Rails.logger.info "Unable to load configuration, using defaults. Reason: #{e}"
        settings_user = {}
      end
      @settings = settings_default.merge settings_user
    end
    @settings
  end

  def self.get(setting)
    @settings ||= self.load
    @settings[setting]
  end

  def self.set(setting, value)
    Rails.logger.debug "Set settings #{setting} #{value}"
    Rails.logger.debug @settings
    self.load unless @settings
    @settings[setting] = value
    Rails.logger.debug @settings
    self.save
  end

  def self.save
    require 'yaml'

    File.open SETTINGS_FILENAME, 'w' do |file|
        Rails.logger.debug @settings
      file.write @settings.to_yaml
    end
  end

end
