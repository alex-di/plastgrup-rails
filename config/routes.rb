Rails.application.routes.draw do

  get 'pages/template1'
  get 'pages/template2'
  get 'pages/search'
  get 'pages/category'
  get 'pages/contacts'
  get 'pages/main'
  get 'pages/alternate'
  get 'pages/about'
  get 'pages/404'
  get 'pages/services'
  get 'pages/popup'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  root 'pages#main'

  get "admin/login" => "sessions#new"
  post 'admin/login' => 'sessions#create', as: :login
  get "admin/logout" => "sessions#destroy", as: :logout

  get 'search' => "categories#search"
    get 'images' => 'images#index'

  get 'categories/show'
  get 'attachments/download/:id' => 'attachments#download', as: :download


    resources :categories, only: [:index, :show]
    resources :products, only: [:index, :show]
    resources :dealers, only: [:index, :show]

  namespace :admin do
    get "/" => "admin#index", as: :root

    post "settings/set" => "admin#set", as: :stng_set
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
    resources :products do
      resources :factoids
      resources :complects
      resources :attachments
      resources :properties
    end
    resources :faqs
    resources :projects
    resources :recommendations

    resources :categories
    resources :dealers

    resources :adcats
    resources :images

    get "settings" => "settings#get"
    post "settings" => "settings#set"
  end
end
